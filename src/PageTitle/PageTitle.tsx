import { Typography } from "@mui/material";
import React from "react";
import { DefaultProps } from "../types/types";
import styles from "./PageTitle.module.scss";

interface Props extends DefaultProps<HTMLElement> {
    title: string;
}

const PageTitle: React.FC<Props> = ({ title, children }) => (
    <div className={styles.title}>
        <Typography variant="h4" gutterBottom component="h4">
            {title}
            <br />
            {children}
        </Typography>
    </div>
);

export default PageTitle;
