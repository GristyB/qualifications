import React from "react";
import PageTitle from "../PageTitle/PageTitle";

const Contact = () => <PageTitle title="CV builder information" />;

export default Contact;
