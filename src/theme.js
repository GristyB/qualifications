import { createTheme, responsiveFontSizes } from "@mui/material";

const theme = responsiveFontSizes(
    createTheme({
        palette: {
            primary: {
                main: "#f64c72",
                light: "#d4c29f",
                dark: "#8d7d5f",
            },
            secondary: {
                main: "#a6a6a6",
                light: "#b7b7b7",
                dark: "#747474",
            },
        },
        components: {
            MuiAppBar: {
                styleOverrides: {
                    root: { backgroundColor: "#2f2fa2" },
                },
            },
            MuiCssBaseline: {
                styleOverrides: `
                  h1 {
                    color: #f64c72;
                  },
                  h2 {
                    color: #f64c72;
                  },
                  h3 {
                    color: #f64c72;
                  },
                  h4 {
                    color: #f64c72;
                  },
                  h5 {
                    color: #f64c72;
                  }
                `,
            },
        },
    })
);

export default theme;
