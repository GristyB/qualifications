import { Link } from "@mui/material";
import React from "react";
import { Link as RouterLink } from "react-router-dom";
import PageTitle from "../PageTitle/PageTitle";

const CvBuilder = () => (
    <PageTitle title="CV builder is coming soon...">
        <Link to="/" component={RouterLink}>
            Home
        </Link>
    </PageTitle>
);

export default CvBuilder;
