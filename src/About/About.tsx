import React from "react";
import PageTitle from "../PageTitle/PageTitle";

const About = () => <PageTitle title="CV builder information" />;

export default About;
