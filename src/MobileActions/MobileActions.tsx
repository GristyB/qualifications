import React from "react";
import PhoneIcon from "@mui/icons-material/Phone";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import { Link } from "@mui/material";
import styles from "./MobileActions.module.scss";
import { DefaultProps } from "../types/types";

const MobileActions: React.FC<DefaultProps<HTMLElement>> = () => (
    <div className={styles.actions}>
        <div className={styles.action}>
            <Link href="tel:00000000000">
                <PhoneIcon />
            </Link>
        </div>
        <div className={styles.action}>
            <Link href="https://api.whatsapp.com/send?phone=074xxxxxx">
                <WhatsAppIcon />
            </Link>
        </div>
    </div>
);

export default MobileActions;
