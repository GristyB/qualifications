import React from "react";
import InstagramIcon from "@mui/icons-material/Instagram";
import FacebookIcon from "@mui/icons-material/Facebook";
import EmailIcon from "@mui/icons-material/Email";
import { Link } from "@mui/material";
import styles from "./SocialMedia.module.scss";

const SocialMedia = () => (
    <div className={styles.socialMedia}>
        <Link className={styles.socialMediaLink} href="/">
            <InstagramIcon />
        </Link>
        <Link className={styles.socialMediaLink} href="/">
            <FacebookIcon />
        </Link>
        <Link className={styles.socialMediaLink} href="/">
            <EmailIcon />
        </Link>
    </div>
);

export default SocialMedia;
