import React from "react";
import MasonryImageList from "../MasonryImageList/MasonryImageList";
import PageTitle from "../PageTitle/PageTitle";

const Home = () => (
    <>
        <PageTitle title="My cool CV builder" />
        <MasonryImageList />
        {[...new Array(20)]
            .map(
                () => `Cras mattis consectetur purus sit amet fermentum.
Cras justo odio, dapibus ac facilisis in, egestas eget quam.
Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
Praesent commodo cursus magna, vel scelerisque nisl consectetur et.`
            )
            .join("\n")}
    </>
);

export default Home;
