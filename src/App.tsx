import React from "react";
import { Container, CssBaseline, ThemeProvider, Toolbar } from "@mui/material";
import { Outlet } from "react-router-dom";
import theme from "./theme";
import ScrollTop from "./ScrollTop/ScrollTop";
import NavBar from "./NavBar/NavBar";
import styles from "./App.module.scss";

const App = () => (
    <>
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <NavBar />
            <Toolbar id="back-to-top-anchor" />
            <Container className={styles.mainContainer}>
                <Outlet />
            </Container>
            <ScrollTop />
        </ThemeProvider>
    </>
);

export default App;
