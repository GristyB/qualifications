import React from "react";
import { Link } from "@mui/material";
import { Link as RouterLink } from "react-router-dom";
import PageTitle from "../PageTitle/PageTitle";

const CoverLetter = () => (
    <PageTitle title="Cover letter is coming soon...">
        <Link to="/" component={RouterLink}>
            Home
        </Link>
    </PageTitle>
);

export default CoverLetter;
