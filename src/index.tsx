import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from "./App";
import Home from "./Home/Home";
import CoverLetter from "./CoverLetter/CoverLetter";
import Contact from "./Contact/Contact";
import About from "./About/About";
import CvBuilder from "./CvBuilder/CvBuilder";

ReactDOM.render(
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<App />}>
                <Route path="/" element={<Home />} />
                <Route path="cvBuilder" element={<CvBuilder />} />
                <Route path="coverLetter" element={<CoverLetter />} />
                <Route path="about" element={<About />} />
                <Route path="contact" element={<Contact />} />
            </Route>
        </Routes>
    </BrowserRouter>,
    document.getElementById("root")
);
